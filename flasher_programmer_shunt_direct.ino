
#define LOW_MS 340
#define HIGH_MS 476
#define LOW_ERROR_MS 20
#define HIGH_ERROR_MS 20
#define SW_NO_SOUND 0
#define SW_OFFSET_1 1 
#define SW_OFFSET_2 2
#define SW_OFFSET_3 3
#define SW_OFFSET_4 4

#define TONE 1000
#define COOLDOWN_MS 0
#define CHANGE_DELAY_MS 100

#define CHANGE_OUT 10
#define SIGNAL_IN 12
#define LED_OUT 9
#define BTN_IN 7
#define SPKR_OUT 11

#define STATE_IDLE 1       // Waiting for button press
#define STATE_SEEK 2       // Looking for base pattern
#define STATE_SKIP 3       // Skipping to desired pattern
#define STATE_MANUAL 4     // Manual mode

//#define PATTERN_STATE_UNDECIDED // Pattern matches so far, but not finished matching
//#define PATTERN_STATE_FAIL      // Pattern failed to match, starting over
//#define PATTERN_STATE_FAIL      
 

#define STATE_ERROR_HIGH -1 // Reset to here on invalid LOW to HIGH transition
#define STATE_INITIAL_LOW 0 // Reset to here on invalid HIGH to LOW transition
#define STATE_1_HIGH 1
#define STATE_2_LOW 2
#define STATE_3_HIGH 3
#define STATE_4_LOW 4
#define STATE_5_HIGH 5
#define STATE_6_LOW 6
#define STATE_7_HIGH 7
#define STATE_8_LOW 8
#define STATE_9_LONG_HIGH 9
#define STATE_SUCCESS_LOW 10

int g_state = STATE_IDLE;

void setup() {
  Serial.begin(9600);
  pinMode(CHANGE_OUT, OUTPUT);
  pinMode(SIGNAL_IN, INPUT);
  pinMode(LED_OUT, OUTPUT);
  pinMode(BTN_IN, INPUT_PULLUP);
  pinMode(SPKR_OUT, OUTPUT);

  pinMode(SW_OFFSET_1, INPUT_PULLUP);
  pinMode(SW_OFFSET_2, INPUT_PULLUP);
  pinMode(SW_OFFSET_3, INPUT_PULLUP);
  pinMode(SW_OFFSET_4, INPUT_PULLUP);

  //g_offset = (3 << (char)!digitalRead(SW_OFFSET_1)) + (2 << (char)!digitalRead(SW_OFFSET_2)) + (1 << (char)!digitalRead(SW_OFFSET_3)) + (char)!digitalRead(SW_OFFSET_4);
  //Serial.print(g_offset);
  //Serial.print(1 digitalRead(SW_OFFSET_1));
  //Serial.print(3 << !digitalRead(SW_OFFSET_4) );


  digitalWrite(CHANGE_OUT, HIGH); // Pull down 12v signal to ground
}

#define LED_MIN_VALUE 1
#define LED_MAX_VALUE 20
#define LED_PERIOD 10000

struct {
  int led_value = LED_MIN_VALUE;
  int led_dir = 1;
  unsigned int led_counter = 0;
  
} g_led_throbing;

char read_offset() {
  char offset = 0;
  
  if(!digitalRead(SW_OFFSET_4)) {
    offset += 1;
  }

  if(!digitalRead(SW_OFFSET_3)) {
    offset += 2;
  }

  if(!digitalRead(SW_OFFSET_2)) {
    offset += 4;
  }

  if(!digitalRead(SW_OFFSET_1)) {
    offset += 8;
  }

  return offset;
}

void throbe_led() {
  g_led_throbing.led_counter++;
  if(g_led_throbing.led_counter == LED_PERIOD) { // led values are changed only at the end of a period to slow it down
    g_led_throbing.led_value += g_led_throbing.led_dir;
    analogWrite(LED_OUT, g_led_throbing.led_value); 
    if (g_led_throbing.led_value == LED_MAX_VALUE) { // change led throb direction depending on where we are in the cycle
      g_led_throbing.led_dir = -1;
    } else if (g_led_throbing.led_value == LED_MIN_VALUE) {
      g_led_throbing.led_dir = 1;
    }
    g_led_throbing.led_counter = 0; // begin new period
  }
}



bool btn_pressed = false; // Current state of the button
bool btn_event = false;   // Queued button release event
bool btn_long_event = false; // Long button press
unsigned long press_time = 0;

void btn_poll() {
  if(btn_pressed) {
    if(digitalRead(BTN_IN)) { // released
      
      btn_pressed = false;
      unsigned long press_dur = millis() - press_time;
      if(press_dur > 1000) {
        btn_long_event = true;
      } else if (press_dur > 20) {
        btn_event = true;
        //Serial.print(millis() - press_time);
      //Serial.print("\n");
      }
    }
  } else {
    if(!digitalRead(BTN_IN)) { // pressed
      press_time = millis();
      btn_pressed = true;
      btn_event = false;
      btn_long_event = false;
    }
  }
}

bool process_btn_event() {
  int e = btn_event;
  btn_event = false;
  return e;
}

bool process_btn_long_event() {
  bool e = btn_long_event;
  if(e) {
    //Serial.print("long\n");
  }
  btn_long_event = false;
  return e;
}

void cycle_pattern() {
  int wait = CHANGE_DELAY_MS;
  unsigned long start_time = millis();
  g_led_throbing.led_value = LED_MIN_VALUE;
  g_led_throbing.led_dir = 1;
  g_led_throbing.led_counter = 0;
  digitalWrite(CHANGE_OUT, LOW);
  while( millis() < start_time + wait) {
    throbe_led();
  }
  digitalWrite(CHANGE_OUT, HIGH);
  analogWrite(LED_OUT, 0); // switch off the led after throb
}

void cycle_n_times(int n) {
  while(n--) {
    cycle_pattern();
    //tone(SPKR_OUT, TONE+400, 50);
    delay(100);
  }
}

// Mirror signal to led
void signal_mirror() {
  int led_signal = digitalRead(SIGNAL_IN);
  if(led_signal == HIGH) {
    analogWrite(LED_OUT, 20);
    //digitalWrite(LED_OUT, HIGH);
  } else {
    analogWrite(LED_OUT, 0);
    //digitalWrite(LED_OUT, LOW);
  }
}

struct {
  bool first = true; // First signal will almost always be cut in the middle, skip validating it
  unsigned long start_time;
  bool started = false;
  int level;
  int matches = 0;
} g_signal_state;

int skip = 0;

void loop() {
  bool h;
  btn_poll();


//  if(process_btn_event()) {
//    tone(SPKR_OUT, TONE, 100);
//    g_state = STATE_SEEK;
//  } 
//
//  
  
  //tone(SPKR_OUT, TONE, 100);
 

  // Take sample
  // Get delta time
  switch(g_state) {
    case STATE_IDLE:
      throbe_led();
      if(process_btn_event()) {
        tone(SPKR_OUT, TONE, 100);
        g_state = STATE_SEEK;
      } 

      if(process_btn_long_event()){
        tone(SPKR_OUT, TONE, 1000);
        g_state = STATE_MANUAL;
      }
      break;
    case STATE_SEEK:
      
      signal_mirror();
      
      if(!g_signal_state.started) {
        g_signal_state.start_time = millis();
        g_signal_state.level = digitalRead(SIGNAL_IN);
        g_signal_state.started = true; // indicate that first period has started and matching can begin
        g_signal_state.first = true; // mark this period as first, because it might be cut off
        g_signal_state.matches = 0;
      } else if(g_signal_state.first && g_signal_state.level != digitalRead(SIGNAL_IN)) {
        g_signal_state.start_time = millis();
        g_signal_state.level = digitalRead(SIGNAL_IN);
        g_signal_state.first = false; // first period is over, new whole signal has began
        //tone(SPKR_OUT, TONE+400, 50);
      } else if(!g_signal_state.first) {
        //tone(SPKR_OUT, TONE-400, 50);
        int new_level = digitalRead(SIGNAL_IN);  
        int signal_period = millis() - g_signal_state.start_time;
        if (new_level != g_signal_state.level) { // signal changed
          //tone(SPKR_OUT, TONE-400, 50);
          g_signal_state.matches++;
          if(g_signal_state.level == LOW) {
            if(signal_period < (LOW_MS - LOW_ERROR_MS)) { // too early
              cycle_pattern();
              //tone(SPKR_OUT, TONE+400, 50);
              delay(COOLDOWN_MS);
              g_signal_state.started = false;
              g_signal_state.matches = 0;
              
            }
          } else { // HIGH
            if(signal_period < (HIGH_MS - HIGH_ERROR_MS)) {
              cycle_pattern();
              //tone(SPKR_OUT, TONE+400, 50);
              delay(COOLDOWN_MS);
              g_signal_state.started = false;
              g_signal_state.matches = 0;
            }
          }

          g_signal_state.start_time = millis();
          g_signal_state.level = digitalRead(SIGNAL_IN);
          
        } else { // signal didn't change
          // too long?
          if(g_signal_state.level == LOW) {
            if(signal_period > (LOW_MS + LOW_ERROR_MS)) {
              //tone(SPKR_OUT, TONE+400, 50);
              cycle_pattern();
              //tone(SPKR_OUT, TONE+400, 50);
              delay(COOLDOWN_MS);
              g_signal_state.started = false;
              g_signal_state.matches = 0;
            }
          } else { // HIGH
            if(signal_period > (HIGH_MS + HIGH_ERROR_MS)) {
              cycle_pattern();
              //tone(SPKR_OUT, TONE+400, 50);
              delay(COOLDOWN_MS);
              g_signal_state.started = false;
              g_signal_state.matches = 0;
            }
          }
        }
        
        if(g_signal_state.matches > 7) {
          g_state = STATE_SKIP;
          for(int i=0;i<5;i++) {
            //tone(SPKR_OUT, TONE+i*100, 70);
            delay(100);
          }
        }
      } else if (g_signal_state.first && (millis() - g_signal_state.start_time) > 600 ) {// timeout for solid
         cycle_pattern();
         //tone(SPKR_OUT, TONE+400, 50);
         delay(COOLDOWN_MS);
         g_signal_state.started = false;
         g_signal_state.matches = 0;
      }
      break;
    case STATE_SKIP:
      //throbe_led();
      cycle_n_times(read_offset());
      g_signal_state.started = false;
      g_state = STATE_IDLE;
      for(int i=0;i<3;i++) {
        tone(SPKR_OUT, TONE, 100);
        analogWrite(LED_OUT, 20); 
        delay(100);
        analogWrite(LED_OUT, 0); 
        delay(100);
      }
      btn_event = false; // prevent double press
      break;
    case STATE_MANUAL:
      if(process_btn_event()) {
        tone(SPKR_OUT, TONE, 100);
        //g_state = STATE_IDLE;
        cycle_pattern();
        delay(COOLDOWN_MS);
      }
      if(process_btn_long_event()){
        tone(SPKR_OUT, TONE, 1000);
        g_state = STATE_IDLE;
      }
      signal_mirror();
      break;
  }
  
  

}
